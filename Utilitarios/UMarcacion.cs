﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

   /*
     @Autor : Yeison Felipe Cifuentes Palacios            
     *Fecha de creación: 27/09/2019
     *Descripcion : Clase que contiene el cascaron de la marcacion.       
   */
namespace Utilitarios
{
    [Serializable]
    [Table("marcacion", Schema = "public")]
    public class UMarcacion
    {
        private long id;
        private long idActividad;
        private long idTema;
        private DateTime fecha;
        private long idUsuario;

        [Key]
        [Column("id")]
        public long Id { get => id; set => id = value; }
        [Column("id_actividad")]
        public long IdActividad { get => idActividad; set => idActividad = value; }
        [Column("id_tema")]
        public long IdTema { get => idTema; set => idTema = value; }
        [Column("fecha")]
        public DateTime Fecha { get => fecha; set => fecha = value; }
        [Column("id_usuario")]
        public long IdUsuario { get => idUsuario; set => idUsuario = value; }


    }
}
