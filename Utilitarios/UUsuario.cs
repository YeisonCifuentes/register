﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 27/09/2019
  *Descripcion : Clase que contiene el cascaron del usuario.       
*/

namespace Utilitarios
{
    [Serializable]
    [Table("usuario", Schema = "public")]
    public class UUsuario
    {
        private long id;
        private long cedula;
        private String nombre;
        private String apellido;
        private long telefono;
        private String rfId;
        private String correo;

        [Key]
        [Column("id")]
        public long Id { get => id; set => id = value; }
        [Column("cedula")]
        public long Cedula { get => cedula; set => cedula = value; }
        [Column("nombre")]
        public string Nombre { get => nombre; set => nombre = value; }
        [Column("apellido")]
        public string Apellido { get => apellido; set => apellido = value; }
        [Column("telefono")]
        public long Telefono { get => telefono; set => telefono = value; }
        [Column("rf_id")]
        public string RfId { get => rfId; set => rfId = value; }
        [Column("correo")]
        public string Correo { get => correo; set => correo = value; }
    }
}
