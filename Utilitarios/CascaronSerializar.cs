﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene el cascaron utilizado para serializar las actividades y temas del usuario.       
*/
namespace Utilitarios
{
    public class CascaronSerializar
    {
        private string temas;
        private string actividades;

      
        public string Temas { get => temas; set => temas = value; }
        public string Actividades { get => actividades; set => actividades = value; }
    }
}
