﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

   /*
     @Autor : Yeison Felipe Cifuentes Palacios            
     *Fecha de creación: 27/09/2019
     *Descripcion : Clase que contiene el cascaron del tema.       
   */
namespace Utilitarios
{
    [Serializable]
    [Table("tema", Schema = "public")]
    public class UTema
    {
        private long id;
        private String nombre;
        private DateTime fechaInicio;
        private DateTime fechaFin;
        private long idProfesor;

        [Key]
        [Column("id")]
        public long Id { get => id; set => id = value; }
        [Column("nombre")]
        public string Nombre { get => nombre; set => nombre = value; }
        [Column("fecha_inicio")]
        public DateTime FechaInicio { get => fechaInicio; set => fechaInicio = value; }
        [Column("fecha_fin")]
        public DateTime FechaFin { get => fechaFin; set => fechaFin = value; }
        [Column("id_profesor")]
        public long IdProfesor { get => idProfesor; set => idProfesor = value; }
    }
}
