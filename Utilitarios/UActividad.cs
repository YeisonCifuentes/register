﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 27/09/2019
  *Descripcion : Clase que contiene el cascaron de la actividad.       
*/

namespace Utilitarios
{
    [Serializable]
    [Table("actividades", Schema = "public")]
    public class UActividad
    {
        private long id;
        private String nombre;

        [Key]
        [Column("id")]
        public long Id { get => id; set => id = value; }
        [Column("nombre")]
        public string Nombre { get => nombre; set => nombre = value; }
    }
}
