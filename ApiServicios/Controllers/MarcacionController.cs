﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Core;

/*
   @Autor : Yeison Felipe Cifuentes Palacios            
   *Fecha de creación: 23/10/2019
   *Descripcion : Clase que contiene todos los servicio necesarios para la Marcacion     
*/
namespace ApiServicios.Controllers
{
    public class MarcacionController : ApiController 
    {

        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 23/10/2019
         *Descripcion : Servicio que permite la insercion de una marcacion.
         *Este metodo recibe : String datosJson (UMarcacion) 
         *Retorna : Mensaje exito o error
        */
        [HttpGet]
        [Route("marcacion/insertar")]
        public string insertar(string datosJson)
        {
            return new CoreMarcacion().insertar(datosJson);
        }
    }
}