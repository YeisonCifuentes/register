﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Core;

/*
   @Autor : Yeison Felipe Cifuentes Palacios            
   *Fecha de creación: 23/10/2019
   *Descripcion : Clase que contiene todos los servicio necesarios para el usuario    
*/
namespace ApiServicios.Controllers
{
    public class UsuarioController : ApiController
    {

        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Servicio que valida que el RfId pertenezca a algun usuario, de ser asi lista las actividades y temas del mismo.
         *Este metodo recibe : String datosJson (rfId) 
         *Retorna : List<UActividad> List<UTema> lista de actividades y temas serializadas en un Json, String mensaje error
        */
        [HttpGet]
        [Route("usuario/validarRfId")]
        public String validarRfId(string datosJson)
        {
            return new CoreUsuario().validarRfId(datosJson);
        }

        //SERVICIO DE PRUEBA
        [HttpGet]
        [Route("prueba")]
        public String prueba()
        {
            return "HOLA DESDE EL SERVICIO :)";
        }
    }
}