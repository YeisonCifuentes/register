﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Utilitarios;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene los metodos para la manipulacion del Usuario.       
*/
namespace DataPostgres
{
    public class DPUsuario
    {
       /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que cambia lista todos los usuarios.         
         *Retorna : List <UUsuario>
      */
        public List<UUsuario> obtenerUsuarios()
        {
            using (var db = new Mapeo())
            {
                return db.usuario.ToList();
            }
        }

        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que obtiene los datos de un solo usuario, filtrando por el RfId.
         *Este metodo recibe : String rfId; 
         *Retorna : UUsuario
        */
        public UUsuario obtenerUser(String rfId)
        {
            using (var db = new Mapeo())
            {
                return db.usuario.Where(user => user.RfId.Equals(rfId)).FirstOrDefault();
            }
        }

        /*
        @Autor : Yeison Felipe Cifuentes Palacios            
        *Fecha de creación: 20/10/2019
        *Descripcion : Metodo que valida que el RfId ingresado exista.
        *Este metodo recibe : String rfId; 
        *Retorna : Boolean
       */
        public Boolean validarRfId(String rfId)
        {
            using (var db = new Mapeo())
            {
                UUsuario usuario = db.usuario.Where(user => user.RfId.Equals(rfId)).FirstOrDefault();
                if (usuario == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}
