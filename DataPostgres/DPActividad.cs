﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios;


/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene los metodos para la manipulacion de la Actividad.       
*/
namespace DataPostgres
{
    public class DPActividad
    {
        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que obtiene las actividades de un usuario, filtrando por el RfId del usuario.
         *Este metodo recibe : String rfId; 
         *Retorna : List<UActividad> 
        */
        public List<UActividad> obtenerActividadesUsuario(String _rfId)
        {
            using (var db = new Mapeo())
            {
                return (from act in db.actividad
                        join marcacion in db.marcacion on act.Id equals marcacion.IdActividad
                        join usuario in db.usuario on marcacion.IdUsuario equals usuario.Id
                        where usuario.RfId.Equals(_rfId)
                        select new
                        {
                            act
                        }
                 ).ToList().Select(acti => new UActividad
                 {
                     Id = acti.act.Id,
                     Nombre = acti.act.Nombre
                     
                 }).ToList();
            }
        }


    }
}
