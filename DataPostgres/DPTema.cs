﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene los metodos para la manipulacion del Tema.       
*/
namespace DataPostgres
{
    public class DPTema
    {

        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que obtiene los temas de un usuario, filtrando por el RfId del usuario.
         *Este metodo recibe : String rfId; 
         *Retorna : List<UTema> 
        */
        public List<UTema> obtenerTemasUsuario(String _rfId)
        {
            using (var db = new Mapeo())
            {
                return (from tema in db.tema
                        join usuario in db.usuario on tema.IdProfesor equals usuario.Id
                        where usuario.RfId.Equals(_rfId)
                        select new
                        {
                            tema
                        }
                 ).ToList().Select(tem => new UTema
                 {
                     Id = tem.tema.Id,
                     Nombre = tem.tema.Nombre,
                     FechaInicio = tem.tema.FechaInicio,
                     FechaFin = tem.tema.FechaFin,
                     IdProfesor= tem.tema.IdProfesor
                 }).ToList();
            }
        }
    }
}
