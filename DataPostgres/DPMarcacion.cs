﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios;


/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene los metodos para la manipulacion de la marcacion.       
*/
namespace DataPostgres
{
    public  class DPMarcacion
    {

        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 23/10/2019
         *Descripcion : Metodo que inserta la marcacion.
         *Este metodo recibe : UMarcacion marcacion, los datos de una marcacion;          
        */
        public void insertarMarcacion(UMarcacion marcacion)
        {
            using (var db = new Mapeo())
            {
                db.marcacion.Add(marcacion);
                db.SaveChanges();
            }
        }

    }
}
