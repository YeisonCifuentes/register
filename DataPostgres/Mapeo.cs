﻿using System.Data.Entity;
using Utilitarios;


namespace DataPostgres
{

    /*
      @Autor : Yeison Felipe Cifuentes Palacios            
      *Fecha de creación: 27/09/2019
      *Descripcion : Clase que contiene el mapeo de las tablas de la BD.       
    */
    public class Mapeo : DbContext
    {
        static Mapeo()
        {
            Database.SetInitializer<Mapeo>(null);
        }
        private readonly string schema;

        public Mapeo()
            : base("name=Conexion")
        {
        }

        public DbSet<UUsuario> usuario { get; set; }
        public DbSet<UActividad> actividad{ get; set; }
        public DbSet<UMarcacion> marcacion { get; set; }
        public DbSet<UTema> tema { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.HasDefaultSchema(this.schema);
            base.OnModelCreating(builder);
        }

    }
}
