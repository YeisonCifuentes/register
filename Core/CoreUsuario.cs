﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataPostgres;
using Utilitarios;



/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene las funciones para el usuario.       
*/
namespace Core
{
    public class CoreUsuario
    {

        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que lista todos lo usuarios.        
         *Retorna : List<UUsuario> lista de usuarios serializados en un Json
        */
        public string listarUsuarios()
        {
            try
            {

                return JsonConvert.SerializeObject(new DPUsuario().obtenerUsuarios());
            }
            catch (Exception ex)
            {

                return ("Error");
            }
        }


        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que obtiene los datos de un usuario, filtrando por el RfId.
         *Este metodo recibe : String datosJson (rfId); 
         *Retorna : UUsuario serializado en un Json
        */
        public string obtenerUsuario(String datosJson)
        {
            try
            {
                UUsuario usuario = JsonConvert.DeserializeObject<UUsuario>(datosJson);

                return JsonConvert.SerializeObject(new DPUsuario().obtenerUser(usuario.RfId));
            }
            catch (Exception ex)
            {

                return ("Error obteniendo los datos del usuario");
            }
        }


        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que valida que el RfId pertenezca a algun usuario, de ser asi lista las actividades y temas del mismo.
         *Este metodo recibe : String datosJson (rfId) 
         *Retorna : List<UActividad> List<UTema> lista de actividades y temas serializadas en un Json, String mensaje error
        */
        public string validarRfId(String datosJson)
        {
            CascaronSerializar serializar = new CascaronSerializar();
            try
            {
                UUsuario usuario = JsonConvert.DeserializeObject<UUsuario>(datosJson);
                
                if (new DPUsuario().validarRfId(usuario.RfId))
                {
                    serializar.Actividades = JsonConvert.SerializeObject(new DPActividad().obtenerActividadesUsuario(usuario.RfId));
                    serializar.Temas = JsonConvert.SerializeObject(new DPTema().obtenerTemasUsuario(usuario.RfId));

                    return JsonConvert.SerializeObject(serializar);
                    
                }
                else
                {
                    return "RfId NO EXISTENTE";
                }
            }
            catch (Exception ex)
            {

                return ("Se presento un error, vuelve a intentarlo");
            }
        }
        
    }
}
