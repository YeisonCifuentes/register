﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios;
using DataPostgres;
using Newtonsoft.Json;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene las funciones para el usuario.       
*/
namespace Core
{
    public class CoreActividad
    {

        /*
        @Autor : Yeison Felipe Cifuentes Palacios            
        *Fecha de creación: 20/10/2019
        *Descripcion : Metodo que obtiene las actividades de un usuario, filtrando por el RfId del usuario.
        *Este metodo recibe : String datosJson (RfId); 
        *Retorna : List<UActividad> lista de actividades serializadas en un Json 
       */
        public string obtenerActividadesUsuario(String datosJson)
        {
            try
            {
                UUsuario usuario = JsonConvert.DeserializeObject<UUsuario>(datosJson);

                return JsonConvert.SerializeObject(new DPActividad().obtenerActividadesUsuario(usuario.RfId));
            }
            catch (Exception ex)
            {

                return ("Error listando");
            }
        }

    }
}
