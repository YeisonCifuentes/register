﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios;
using DataPostgres;
using Newtonsoft.Json;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 20/10/2019
  *Descripcion : Clase que contiene las funciones para el tema.       
*/
namespace Core
{
    public class CoreTema
    {
        /*
         @Autor : Yeison Felipe Cifuentes Palacios            
         *Fecha de creación: 20/10/2019
         *Descripcion : Metodo que obtiene los temas de un usuario, filtrando por el RfId del usuario.
         *Este metodo recibe : String datosJson (RfId); 
         *Retorna : List<UTema> lista de temas selializadas en un Json
        */
        public string obtenerTemasUsuario(String datosJson)
        {
            try
            {
                UUsuario usuario = JsonConvert.DeserializeObject<UUsuario>(datosJson);

                return JsonConvert.SerializeObject(new DPTema().obtenerTemasUsuario(usuario.RfId));
            }
            catch (Exception ex)
            {

                return ("Error listando");
            }
        }
    }
}
