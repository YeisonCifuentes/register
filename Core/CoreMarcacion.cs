﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios;
using DataPostgres;
using Newtonsoft.Json;

/*
  @Autor : Yeison Felipe Cifuentes Palacios            
  *Fecha de creación: 23/10/2019
  *Descripcion : Clase que contiene las funciones para la marcacion.       
*/
namespace Core
{
    public class CoreMarcacion
    {


        /*
        @Autor : Yeison Felipe Cifuentes Palacios            
        *Fecha de creación: 23/10/2019
        *Descripcion : Metodo que inserta la marcacion.
        *Este metodo recibe : String datosJson, los datos de la marcacion, UMarcacion; 
        *Retorna : Mensaje de exito o error
       */
        public string insertar(String datosJson)
        {           
            try
            {
                UMarcacion marcacion = JsonConvert.DeserializeObject<UMarcacion>(datosJson);
                new DPMarcacion().insertarMarcacion(marcacion);
                return "REGISTRO EXITOSO";
            }
            catch (Exception ex)
            {
               return "REGISTRO ERRONEO, INTENTE NUEVAMENTE";
            }
        }
    }
}
